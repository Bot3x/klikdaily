import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import logo from './src/asset/otong.png';
import apple from './src/asset/apple.png';

export default class App extends Component {
  constructor() {
    super();
  }
  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <ScrollView>
          <View style={styles.container}>
            <View>
              <View>
                <Text
                  style={{fontWeight: 'bold', fontSize: 20, marginLeft: 20}}>
                  Tanggerang,Indonesia
                </Text>
              </View>
              <View style={{alignSelf: 'flex-end'}}>
                <View>
                  <Image
                    style={{
                      height: 50,
                      width: 50,
                      borderRadius: 30,
                      marginTop: 5,
                    }}
                    source={{
                      uri: 'https://ephor.files.wordpress.com/2015/04/makan-buah.jpeg',
                    }}
                  />
                </View>
              </View>
            </View>
          </View>
          <View style={{flexDirection: 'row'}}>
            <Text style={{color: 'yellow', fontSize: 28, marginLeft: 20}}>
              Find
            </Text>
            <Text style={{color: 'green', fontSize: 28}}> Fresh Groceries</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginLeft: 20,
              backgroundColor: '#E5E5E5',
              borderRadius: 15,
              marginBottom: 15,
              marginTop: 15,
            }}>
            <Icon name="magnifying-glass" size={30} color="black" />
            <TextInput
              placeholder="Search Groceries"
              style={{fontSize: 16, marginLeft: 20}}
            />
          </View>
          <Text style={{fontSize: 16, fontWeight: 'bold', marginLeft: 20}}>
            Categories
          </Text>
          {/* <View style={{flexDirection: 'row'}}>
            <View style={{marginLeft: 30, marginRight: 30}}>
              <Text
                style={{
                  backgroundColor: 'green',
                  borderRadius: 10,
                  height: 39,
                  width: 98,
                  fontSize: 16,
                  marginTop: 21,
                  color: 'white',
                  fontWeight: 'bold',
                }}>
                Apple
              </Text>
            </View>
            <View style={{marginLeft: 20, marginRight: 20}}>
              <Text
                style={{
                  backgroundColor: 'yellow',
                  borderRadius: 10,
                  height: 39,
                  width: 98,
                  fontSize: 16,
                  marginTop: 21,
                  color: 'white',
                  fontWeight: 'bold',
                }}>
                Banana
              </Text>
            </View>
            <View style={{marginLeft: 20, marginRight: 20}}>
              <Text
                style={{
                  backgroundColor: 'orange',
                  borderRadius: 10,
                  height: 39,
                  width: 98,
                  fontSize: 16,
                  marginTop: 21,
                  color: 'white',
                  fontWeight: 'bold',
                }}>
                Orange
              </Text>
            </View>
          </View> */}
          <View style={{flexDirection: 'row'}}>
            <View style={styles.box}>
              <TouchableOpacity>
                <View style={styles.salt}>
                  <Text
                    style={{
                      fontSize: 20,
                      fontWeight: 'bold',
                      color: 'white',
                      backgroundColor: 'green',
                      borderRadius: 10,
                      width: 98,
                      height: 39,
                    }}>
                    Apple
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={styles.box}>
              <TouchableOpacity>
                <View style={styles.salt}>
                  <Text
                    style={{
                      fontSize: 20,
                      fontWeight: 'bold',
                      backgroundColor: '#E5E5E5',
                      borderRadius: 10,
                      width: 98,
                      height: 39,
                    }}>
                    Orange
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={styles.box}>
              <TouchableOpacity>
                <View style={styles.salt}>
                  <Text
                    style={{
                      fontSize: 20,
                      fontWeight: 'bold',
                      backgroundColor: '#E5E5E5',
                      borderRadius: 10,
                      width: 98,
                      height: 39,
                    }}>
                    Banana
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              marginTop: 17,
              flexDirection: 'row',
              borderRadius: 10,
              backgroundColor: '#FFFFFF',
              elevation: 1,
              marginLeft: 35,
              width: 344,
              height: 105,
            }}>
            <View>
              <Image
                style={{height: 77, width: 78, backgroundColor: '#E5E5E5'}}
                source={apple}
              />
            </View>
            <View>
              <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                Apple Sweet Indonesia
              </Text>
              <Text style={{fontSize: 21, fontWeight: 'bold'}}>
                Rp 25.000/kg
              </Text>
              <View style={{flexDirection: 'row'}}>
                <Image style={styles.logo} source={logo} />
                <Image style={styles.logo} source={logo} />
                <Image style={styles.logo} source={logo} />
                <Image style={styles.logo} source={logo} />
              </View>
            </View>
          </View>
          <View
            style={{
              marginTop: 17,
              flexDirection: 'row',
              borderRadius: 10,
              backgroundColor: '#FFFFFF',
              elevation: 1,
              marginLeft: 35,
              width: 344,
              height: 105,
            }}>
            <View>
              <Image
                style={{height: 77, width: 78, backgroundColor: '#E5E5E5'}}
                source={apple}
              />
            </View>
            <View>
              <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                Apple Sweet Canada
              </Text>
              <Text style={{fontSize: 21, fontWeight: 'bold'}}>
                Rp 15.000/kg{' '}
              </Text>
              <View style={{flexDirection: 'row'}}>
                <Image style={styles.logo} source={logo} />
                <Image style={styles.logo} source={logo} />
                <Image style={styles.logo} source={logo} />
              </View>
            </View>
          </View>
          <View
            style={{
              marginTop: 17,
              flexDirection: 'row',
              borderRadius: 10,
              backgroundColor: '#FFFFFF',
              elevation: 1,
              marginLeft: 35,
              width: 344,
              height: 105,
            }}>
            <View>
              <Image
                style={{height: 77, width: 78, backgroundColor: '#E5E5E5'}}
                source={apple}
              />
            </View>
            <View>
              <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                Apple Sweet Japan
              </Text>
              <Text style={{fontSize: 21, fontWeight: 'bold'}}>
                {' '}
                Rp 20.000/kg{' '}
              </Text>
              <View style={{flexDirection: 'row'}}>
                <Image style={styles.logo} source={logo} />
                <Image style={styles.logo} source={logo} />
                <Image style={styles.logo} source={logo} />
              </View>
            </View>
          </View>
          <View
            style={{
              marginTop: 17,
              flexDirection: 'row',
              borderRadius: 10,
              backgroundColor: '#FFFFFF',
              elevation: 1,
              marginLeft: 35,
              width: 344,
              height: 105,
            }}>
            <View>
              <Image
                style={{height: 77, width: 78, backgroundColor: '#E5E5E5'}}
                source={apple}
              />
            </View>
            <View>
              <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                Apple Sweet India
              </Text>
              <Text style={{fontSize: 21, fontWeight: 'bold'}}>
                {' '}
                Rp 10.000/kg{' '}
              </Text>
              <View style={{flexDirection: 'row'}}>
                <Image style={styles.logo} source={logo} />
                <Image style={styles.logo} source={logo} />
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {},
  box: {
    justifyContent: 'center',
    marginTop: 10,
    marginLeft: 35,
    height: 39,
    width: 98,
    alignSelf: 'center',
  },
  salt: {},
});
